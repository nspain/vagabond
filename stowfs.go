package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"
)

type StowFSBackend interface {
	// Needed for general file system access
	afero.Fs
	// Needed for following symlinks
	afero.LinkReader
	// Needed for creating symlinks
	afero.Linker
	// Needed for querying the status of files and symlinks
	afero.Lstater
}

// StowFS is a file system abstraction that provides the necessities required to
// perform the various stow actions.
type StowFS struct {
	fs     StowFSBackend
	dest   string
	source string
}

// NewStowFS creates a new StowFS object with the given destination and source.
func NewStowFS(fs StowFSBackend, dest, source string) *StowFS {
	return &StowFS{
		fs:     fs,
		dest:   dest,
		source: source,
	}
}

// inDomain checks if `path` is in the StowFS's domain.
func (fs *StowFS) inDomain(path string) bool {
	return strings.HasPrefix(path, fs.dest) || strings.HasPrefix(path, fs.source)
}

// isWritable checks if `path` is writable within the StowFS instance.
func (fs *StowFS) isWritable(path string) bool {
	return strings.HasPrefix(path, fs.dest)
}

// Exists checks if `path` exists to the view of the StowFS instance.
func (fs *StowFS) Exists(path string) (bool, error) {
	if !fs.inDomain(path) {
		return false, nil
	}
	if exists, err := afero.Exists(fs.fs, path); err != nil {
		return false, err
	} else {
		return exists, nil
	}
}

// DirExists checks if `path` exists to the view of the StowFS instance.
func (fs *StowFS) DirExists(path string) (bool, error) {
	if !fs.inDomain(path) {
		return false, nil
	}

	if exists, err := afero.DirExists(fs.fs, path); err != nil {
		return false, err
	} else {
		return exists, nil
	}
}

type domainError struct {
	path string
}

func (e domainError) Error() string {
	return fmt.Sprintf("%s is not in the domain of this stowfs instance", e.path)
}

// IsDomainError checks if `e` is a domain error. This indicates that
// there was the attemp to perform an action outside of the StowFS
// instance's domain.
func IsDomainError(e error) bool {
	_, ok := e.(domainError)
	return ok
}

func newDomainError(path string) error {
	return domainError{path}
}

type writableError struct {
	path string
}

func (e writableError) Error() string {
	return fmt.Sprintf("%s is not writable by this stowfs instance", e.path)
}

// IsWritableError checks if `e` is a writable error. This indicates
// that there was an attempt to write to something (directory, file,
// symlink) that is in the StowFS instance's domain but not the writable part.
func IsWritableError(e error) bool {
	_, ok := e.(writableError)
	return ok
}

func newWritableError(path string) error {
	return writableError{path}
}

// Symlink creates a symlink from `from` to `to`.
func (fs *StowFS) Symlink(from, to string) error {
	if !fs.inDomain(from) {
		return newDomainError(from)
	} else if !fs.inDomain(to) {
		return newDomainError(to)
	} else if !fs.isWritable(to) {
		return newWritableError(to)
	}
	return fs.fs.SymlinkIfPossible(from, to)
}

// IsSmylink checks if `path` is a symlink.
func (fs *StowFS) IsSymlink(path string) (bool, error) {
	if !fs.inDomain(path) {
		return false, nil
	}

	// os.Lstate must be used, instead of os.Stat, because Lstat
	// doesn't follow the link.
	if stat, _, err := fs.fs.LstatIfPossible(path); os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	} else if stat.Mode()&os.ModeSymlink == 0 {
		return false, nil
	}
	return true, nil
}

func walk(fs *StowFS, path string, info os.FileInfo, walkFn filepath.WalkFunc) error {
	if err := walkFn(path, info, nil); err != nil {
		if info.IsDir() && err == filepath.SkipDir {
			return nil
		}
		return err
	}
	if !info.IsDir() {
		return nil
	}

	fh, err := fs.fs.Open(path)
	if err != nil {
		return err
	}
	defer fh.Close()

	names, err := fh.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		fileName := filepath.Join(path, name)
		info, _, err := fs.fs.LstatIfPossible(fileName)
		if err != nil {
			if err := walkFn(fileName, info, err); err != nil && err != filepath.SkipDir {
				return err
			}
		} else {
			if err := walk(fs, fileName, info, walkFn); err != nil {
				if !info.IsDir() || err != filepath.SkipDir {
					return err
				}
			}

		}
	}
	return nil
}

// Walk walks down the directory tree from `root`.
func (fs *StowFS) Walk(root string, walkFn filepath.WalkFunc) error {
	info, _, err := fs.fs.LstatIfPossible(root)
	if err != nil {
		return walkFn(root, nil, err)
	}
	return walk(fs, root, info, walkFn)
}
