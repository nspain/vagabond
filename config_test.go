package main

import (
	"flag"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"testing"
)

func (c Config) Equal(o Config) bool {
	return reflect.DeepEqual(c, o)
}

func stringArrayEqual(a1 []string, a2 []string) bool {
	if len(a1) != len(a2) {
		return false
	}

	for i, x := range a1 {
		if x != a2[i] {
			return false
		}
	}
	return true
}

func TestSetConfig(t *testing.T) {
	flagSet := flag.NewFlagSet("test", flag.ExitOnError)
	dirArg := "test_dir"
	targetArg := "test_target"
	stowArg := "test_stow"
	deleteArg := "test_delete"
	restowArg := "test_restow"
	ignoreArg := regexp.MustCompile("ignore")
	deferArg := regexp.MustCompile("defer")
	overrideArg := regexp.MustCompile("override")
	conf, err := getConfigFromCmdline(flagSet, []string{
		"vagabond",
		"-no",
		"-dir", dirArg,
		"-target", targetArg,
		"-verbose",
		"-stow", stowArg,
		"-delete", deleteArg,
		"-restow", restowArg,
		"-ignore", ignoreArg.String(),
		"-defer", deferArg.String(),
		"-override", overrideArg.String(),
		"-dotfiles",
		"-version",
	})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}
	expectedConfig := Config{
		DryRun:         true,
		WorkingDir:     dirArg,
		TargetDir:      targetArg,
		StowPackages:   []string{stowArg},
		DeletePackages: []string{deleteArg},
		RestowPackages: []string{restowArg},
		Ignore:         ignoreArg,
		Defer:          deferArg,
		Override:       overrideArg,
		DotFiles:       true,
		Verbose:        true,
		ShowVersion:    true,
	}
	if !conf.Equal(expectedConfig) {
		t.Fatalf("Expected %v, got %v", expectedConfig, conf)
	}
}

func TestAddMultipleStow(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-stow", "test1", "-stow", "test2", "-stow", "test3"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}

	expected := []string{"test1", "test2", "test3"}
	if !stringArrayEqual(conf.StowPackages, expected) {
		t.Fatalf("Expected conf.StowPackages to be %v but got %v", expected, conf.StowPackages)
	}
}

func TestAddMultipleDelete(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-delete", "test1", "-delete", "test2", "-delete", "test3"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}

	expected := []string{"test1", "test2", "test3"}
	if !stringArrayEqual(conf.DeletePackages, expected) {
		t.Fatalf("Expected conf.DeletePackages to be %v but got %v", expected, conf.DeletePackages)
	}
}

func TestAddMultipleRestow(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-restow", "test1", "-restow", "test2", "-restow", "test3"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}

	expected := []string{"test1", "test2", "test3"}
	if !stringArrayEqual(conf.RestowPackages, expected) {
		t.Fatalf("Expected conf.RestowPackages to be %v but got %v", expected, conf.DeletePackages)
	}
}

func TestErrOnInvalidIgnoreRegexp(t *testing.T) {
	ignoreRegexp := "*"
	_, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-ignore", ignoreRegexp, "test"})
	if err == nil {
		t.Fatalf("Expected returned error on invalid 'ignore' regexp: %s", ignoreRegexp)
	}
}

func TestErrOnInvalidDeferRegexp(t *testing.T) {
	_, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-defer", "*", "test"})
	if err == nil {
		t.Fatalf("Expected returned error on invalid 'defer' regexp")
	}
}

func TestErrOnInvalidOverrideRegexp(t *testing.T) {
	_, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "-override", "*", "test"})
	if err == nil {
		t.Fatalf("Expected returned error on invalid 'override' regexp")
	}
}

func TestErrOnNoStowsDeletesOrRestows(t *testing.T) {
	_, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond"})
	if err == nil {
		t.Fatalf("Expected returned error on missing stow, delete or restow packages")
	}
}

func TestStowPositionalArgs(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "test1", "test2", "test3"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}

	expected := []string{"test1", "test2", "test3"}
	if !stringArrayEqual(conf.StowPackages, expected) {
		t.Fatalf("Expected conf.StowPackages to be %v but got %v", expected, conf.StowPackages)
	}
}

func TestDefaultWorkingDirToCurrentDir(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "test"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}
	wd, _ := os.Getwd()
	if conf.WorkingDir != wd {
		t.Fatalf("Expected conf.WorkingDir to be %v, got %v", wd, conf.WorkingDir)
	}
}

func TestDefaultTargetDirToParentOfCurrentDir(t *testing.T) {
	conf, err := getConfigFromCmdline(&flag.FlagSet{}, []string{"vagabond", "test"})
	if err != nil {
		t.Fatalf("Unexpected failure with error: %v", err)
	}
	wd, _ := os.Getwd()
	parentWd := filepath.Dir(wd)
	if conf.TargetDir != parentWd {
		t.Fatalf("Expected conf.TargetDir to be %v, got %v", wd, conf.TargetDir)
	}
}
