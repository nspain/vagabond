package main

import (
	"fmt"
	"os"
)


// Fail with a formattable error message printed with a trailing newline and an
// exit code of 1.
func fail(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format+"\n", a...)
	os.Exit(1)
}

func warn(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format+"\n", a...)
}
