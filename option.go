package main

import (
	"regexp"
)

type Option struct {
	name  string
	value interface{}
}

func Ignore(re *regexp.Regexp) Option {
	return Option{
		name:  "ignore",
		value: re,
	}
}

func Defer(re *regexp.Regexp) Option {
	return Option{
		name:  "defer",
		value: re,
	}
}

func Override(re *regexp.Regexp) Option {
	return Option{
		name:  "override",
		value: re,
	}
}

func HandleDotFiles() Option {
	return Option{
		name:  "handleDotFiles",
		value: nil,
	}
}
