package main

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/spf13/afero"
)

type FileState int

const (
	File FileState = iota
	Symlink
)

type memMapFs struct {
	fs       afero.Fs
	symlinks map[string]string
}

func newMemMapFs() *memMapFs {
	fs := afero.NewMemMapFs()
	return &memMapFs{
		fs: fs,
		symlinks: make(map[string]string),
	}
}

// Create creates a file in the filesystem, returning the file and an
// error, if any happens.
func (fs *memMapFs) Create(name string) (afero.File, error) {
	return fs.fs.Create(name)
}

// Mkdir creates a directory in the filesystem, return an error if any
// happens.
func (fs *memMapFs) Mkdir(name string, perm os.FileMode) error {
	return fs.fs.Mkdir(name, perm)
}

// MkdirAll creates a directory path and all parents that does not exist
// yet.
func (fs *memMapFs) MkdirAll(path string, perm os.FileMode) error {
	return fs.fs.MkdirAll(path, perm)
}

// Open opens a file, returning it or an error, if any happens.
func (fs *memMapFs) Open(name string) (afero.File, error) {
	return fs.fs.Open(name)
}

// OpenFile opens a file using the given flags and the given mode.
func (fs *memMapFs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {
	return fs.fs.OpenFile(name, flag, perm)
}

// Remove removes a file identified by name, returning an error, if any
// happens.
func (fs *memMapFs) Remove(name string) error {
	return fs.fs.Remove(name)
}

// RemoveAll removes a directory path and any children it contains. It
// does not fail if the path does not exist (return nil).
func (fs *memMapFs) RemoveAll(path string) error {
	return fs.fs.RemoveAll(path)
}

// Rename renames a file.
func (fs *memMapFs) Rename(oldname string, newname string) error {
	return fs.fs.Rename(oldname, newname)
}

// Stat returns a FileInfo describing the named file, or an error, if any
// happens.
func (fs *memMapFs) Stat(name string) (os.FileInfo, error) {
	return fs.fs.Stat(name)
}

// The name of this FileSystem
func (fs *memMapFs) Name() string {
	return fs.fs.Name()
}

//Chmod changes the mode of the named file to mode.
func (fs *memMapFs) Chmod(name string, mode os.FileMode) error {
	return fs.fs.Chmod(name, mode)
}

//Chtimes changes the access and modification times of the named file
func (fs *memMapFs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	return fs.fs.Chtimes(name, atime, mtime)
}

func (fs *memMapFs) ReadlinkIfPossible(name string) (string, error) {
	link, ok := fs.symlinks[name]
	if !ok {
		return "", fmt.Errorf("not a symlink")
	}
	return link, nil
}

func (fs *memMapFs) SymlinkIfPossible(oldname string, newname string) error {
	fs.symlinks[newname] = oldname
	_, err := fs.fs.Create(newname)
	return err 
}

type fileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
	isDir   bool
	sys     interface{}
}

func (fi *fileInfo) Name() string {
	return fi.name
}

func (fi *fileInfo) Size() int64 {
	return fi.size
}

func (fi *fileInfo) Mode() os.FileMode {
	return fi.mode
}

func (fi *fileInfo) ModTime() time.Time {
	return fi.modTime
}

func (fi *fileInfo) IsDir() bool {
	return fi.isDir
}

func (fi *fileInfo) Sys() interface{} {
	return fi.sys
}

func (fs *memMapFs) LstatIfPossible(name string) (os.FileInfo, bool, error) {
	info, err := fs.fs.Stat(name)
	if err != nil {
		return nil, false, err
	}
	fi := &fileInfo{
		name:    info.Name(),
		size:    info.Size(),
		mode:    info.Mode(),
		modTime: info.ModTime(),
		isDir:   info.IsDir(),
		sys:     info.Sys(),
	}
	if fs.symlinks[name] != "" {
		fi.mode |= os.ModeSymlink
	}
	return fi, false, err
}

func assertLinked(t *testing.T, fs *memMapFs, from, to string) {
	if exists, _ := afero.Exists(fs, from); exists {
		if stat, _, _ := fs.LstatIfPossible(from); stat.Mode()&os.ModeSymlink == 0 {
			t.Errorf("Expected %s to be a symlink", from)
		} else {
			link, err := fs.ReadlinkIfPossible(from)
			if err != nil {
				t.Errorf("Failed to read symlink at %s: %v", from, err)
			} else if link != to {
				t.Errorf("Expected %s to link to %s but it links to %s", from, to, link)
			}
		}
	} else {
		t.Errorf("Expected %s to exist", from)
	}
}

func TestStowerStow(t *testing.T) {
	fs := newMemMapFs()
	fs.Mkdir("source", 0777)
	fs.Mkdir("source/test", 0777)
	fs.Create("source/test/file1")
	fs.Create("source/test/file2")
	fs.Mkdir("source/test/dir1", 0777)
	fs.Create("source/test/dir1/file3")
	fs.Create("source/test/dir1/file4")

	fs.Mkdir("dest", 0777)

	stowFS := NewStowFS(fs, "dest", "source")
	stow := NewStow(stowFS, "dest", "source")
	if err := stow.Stow("test"); err != nil {
		t.Fatalf("Unexpected stow failure: %v", err)
	}
	assertLinked(t, fs, "dest/file1", "source/test/file1")
	assertLinked(t, fs, "dest/file2", "source/test/file2")
	assertLinked(t, fs, "dest/dir1/file3", "source/test/dir1/file3")
	assertLinked(t, fs, "dest/dir1/file4", "source/test/dir1/file4")
}

func TestStowerStowDotFiles(t *testing.T) {
	fs := newMemMapFs()
	fs.Mkdir("source", 0777)
	fs.Mkdir("source/test", 0777)
	fs.Create("source/test/dot-bashrc")
	fs.Mkdir("source/test/dot-dir1", 0777)
	fs.Create("source/test/dot-dir1/file3")
	fs.Create("source/test/dot-dir1/file4")

	fs.Mkdir("dest", 0777)

	stowFS := NewStowFS(fs, "dest", "source")
	stow := NewStow(stowFS, "dest", "source", HandleDotFiles())
	if err := stow.Stow("test"); err != nil {
		t.Fatalf("Unexpected stow failure: %v", err)
	}
	assertLinked(t, fs, "dest/.bashrc", "source/test/dot-bashrc")
	assertLinked(t, fs, "dest/.dir1/file3", "source/test/dot-dir1/file3")
	assertLinked(t, fs, "dest/.dir1/file4", "source/test/dot-dir1/file4")
}

func TestStowerStowIgnore(t *testing.T) {

}

func TestStowerStowDefer(t *testing.T) {

}

func TestStowerStowOverride(t *testing.T) {

}
