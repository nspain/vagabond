package main

import (
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"testing"

	"github.com/spf13/afero"
)

func randString(n int) string {
	letterBytes := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func cleanupDirs(t *testing.T, dirs ...string) func() {
	return func() {
		for _, dir := range dirs {
			err := os.RemoveAll(dir)
			if err != nil {
				t.Fatalf("Failed to remove directory %s", dir)
			}
		}
	}
}

func TestNewStowFS(t *testing.T) {
	dest := "dest"
	target := "target"
	osFs := afero.NewOsFs().(*afero.OsFs)
	fs := NewStowFS(osFs, dest, target)
	if fs.dest != dest {
		t.Errorf("Expected fs.dest to be %s, got %s", dest, fs.dest)
	}

	if fs.source != target {
		t.Errorf("Expected fs.target to be %s, got %s", target, fs.source)
	}
}

func TestStowFSExists(t *testing.T) {
	tempDir, err := ioutil.TempDir("", "dir")
	if err != nil {
		t.Fatalf("Failed to create temp dir: %v", err)
	}
	tmpFile, err := ioutil.TempFile(tempDir, "file")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v", err)
	}
	defer tmpFile.Close()
	t.Cleanup(cleanupDirs(t, tempDir))

	osFs := afero.NewOsFs().(*afero.OsFs)
	fs := NewStowFS(osFs, tempDir, tempDir)
	if exists, _ := fs.Exists(tmpFile.Name()); !exists {
		t.Errorf("Expected Exists to be true for %s but it false", tmpFile.Name())
	}

	nonExistant := tmpFile.Name() + randString(5)
	if exists, _ := fs.Exists(nonExistant); exists {
		t.Errorf("Expected Exists to be false for %s but it was true", nonExistant)
	}
}

func TestStowFSDirExists(t *testing.T) {
	tempDir, err := ioutil.TempDir("", "dir")
	if err != nil {
		t.Fatalf("Failed to create temp dir: %v", err)
	}

	innerTempDir1, err := ioutil.TempDir(tempDir, "dir")
	if err != nil {
		t.Fatalf("Failed to create temp dir: %v", err)
	}

	osFs := afero.NewOsFs().(*afero.OsFs)
	fs := NewStowFS(osFs, tempDir, tempDir)
	if exists, _ := fs.DirExists(innerTempDir1); !exists {
		t.Errorf("Expected DirExists to be true for %s but it is false", innerTempDir1)
	}

	innerTempDir2, err := ioutil.TempDir(tempDir, "dir")
	if err != nil {
		t.Fatalf("Failed to create temp dir: %v", err)
	}
	if err := os.Remove(innerTempDir2); err != nil {
		t.Fatalf("Failed to remove temp dir %s: %v", innerTempDir2, err)
	}
	if exists, _ := fs.DirExists(innerTempDir2); exists {
		t.Errorf("Expected DirExists to be false for %s but it is true", innerTempDir2)
	}

	innerTempFile, err := ioutil.TempFile(tempDir, "file")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v", err)
	}
	defer innerTempFile.Close()
	if exists, _ := fs.DirExists(innerTempFile.Name()); exists {
		t.Errorf("Expected DirExists for file %s to be false but it is true", innerTempFile.Name())
	}
}

func TestStowFSSymlink(t *testing.T) {
	tempDir, err := ioutil.TempDir("", "dir")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	tempFile, err := ioutil.TempFile(tempDir, "file")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v", err)
	}
	tempFile.Close()

	osFs := afero.NewOsFs().(*afero.OsFs)
	fs := NewStowFS(osFs, tempDir, tempDir)
	
	symlinkFileName := tempFile.Name() + randString(5)
	if err := fs.Symlink(tempFile.Name(), symlinkFileName); err != nil {
		t.Fatalf("Failed to create symlink from %s to %s: %v", tempFile.Name(), symlinkFileName, err)
	}

	stat, err := os.Lstat(symlinkFileName)
	if err != nil {
		t.Fatalf("Failed to stat %s: %v", symlinkFileName, err)
	}

	if stat.Mode()&os.ModeSymlink == 0 {
		t.Fatalf("Expected %s to be a symlink", symlinkFileName)
	}

	link, err := filepath.EvalSymlinks(symlinkFileName)
	if err != nil {
		t.Fatalf("Failed to evaluate symlink %s: %v", symlinkFileName, err)
	}
	if link != tempFile.Name() {
		t.Fatalf("Expected %s to link to %s but it links to %s", symlinkFileName, tempFile.Name(), link)
	}
}

func TestStowFSIsSymlink(t *testing.T) {
	tempDir, err := ioutil.TempDir("", "dir")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v", err)
	}
	tempFile, err := ioutil.TempFile(tempDir, "file")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v", err)
	}
	tempFile.Close()

	osFs := afero.NewOsFs().(*afero.OsFs)
	fs := NewStowFS(osFs, tempDir, tempDir)
	symlinkFileName := tempFile.Name() + randString(5)
	if err := os.Symlink(tempFile.Name(), symlinkFileName); err != nil {
		t.Fatalf("Failed to symlink %s to %s", symlinkFileName, tempFile.Name())
	}

	if isSymlink, err := fs.IsSymlink(symlinkFileName); err != nil {
		t.Errorf("Failed to check symlink %s: %v", symlinkFileName, err)
	} else if !isSymlink {
		t.Errorf("Expected %s to be a symlink", symlinkFileName)
	}

	if isSymlink, _ := fs.IsSymlink(tempFile.Name()); isSymlink {
		t.Errorf("Expected %s to not be a symlink", tempFile.Name())
	}
}
