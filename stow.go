package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"
)

type Existential interface {
	// Exists checks if `path` exists.
	Exists(path string) bool
	// DirExists checks if `path` exists and is a directory.
	DirExists(path string) bool
}

type Linker interface {
	// Symlink symlinks `from` to `to`.
	Symlink(to, from string) error
	// IsSymlink checks if `path` is a symlink.
	IsSymlink(path string) bool
}

type Remover interface {
	// Remove removes `path`.
	Remove(path string) error
}

type Stower interface {
	Existential
	Linker
}

type Deleter interface {
	Existential
	Remover
}

type Restower interface {
	Existential
	Linker
	Remover
}

type Stow struct {
	fs     *StowFS
	source string
	dest   string
	opts   []Option
}

func NewStow(fs *StowFS, dest, source string, opts ...Option) Stow {
	return Stow{
		fs: fs, source: source, dest: dest, opts: opts,
	}
}

func (s Stow) Stow(packageName string) error {
	packageSourcePath := filepath.Join(s.source, packageName)

	err := s.fs.Walk(s.source, func(pth string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		target := strings.TrimPrefix(pth, packageSourcePath)
		target = filepath.Join(s.dest, target)
		if exists, _ := s.fs.Exists(target); !exists {
			log.Printf("%s has not yet been stowed", pth)
			err := s.fs.Symlink(pth, target)
			if err != nil {
				warn("Failed to link %s to %s: %v", pth, target, err)
			}
			log.Printf("linked %s to %s", pth, target)
		} else if isSymlink, _ := s.fs.IsSymlink(target); isSymlink {
			resolvedTarget, err := filepath.EvalSymlinks(target)
			if err != nil {
				log.Printf("Symlink target %s cannot be resolved: %v", target, err)
				return nil
			}
			log.Printf("Target %s is an existing symlink pointing to %s", target, resolvedTarget)
			if strings.HasPrefix(resolvedTarget, packageSourcePath) {
				log.Printf("Target %s is symlinked to %s which is owned by package %s", target, resolvedTarget, packageName)
			} else {
				targetSource, err := filepath.Rel(packageSourcePath, resolvedTarget)
				if err != nil {
					warn("Target %s is a symlink to %s which is managed outside of %s", target, resolvedTarget, packageSourcePath)
					return nil
				}
				parts := strings.Split(targetSource, string(os.PathSeparator))
				owningPackage := parts[0]
				warn("Target %s is already managed by package %s", owningPackage)
			}
		} else {
			warn("Cannot stow %s as %s already exists", pth, target)
		}

		return nil
	})

	if err != nil {
		return fmt.Errorf("failed to stow package %s: %v", packageName, err)
	}
	return nil
}

func deletePkg(pkg, source, dest string) error {
	return filepath.Walk(dest, func(pth string, info os.FileInfo, err error) error {
		if isSymlink(pth) {
			resolvedPth, err := filepath.EvalSymlinks(pth)
			if err != nil {
				warn("Failed to follow symlink %s: %v", pth, err)
				return nil
			}
			if strings.HasPrefix(resolvedPth, source) {
				log.Printf("Found symlink managed by package %s", pkg)
				if err := os.Remove(pth); err != nil {
					warn("Failed to remove symlink %s to %s: %v", pth, resolvedPth, err)
				}
			} else {
				log.Printf("Found symlink not managed by package %s", pkg)
			}
		}
		return nil
	})
}

func restowPkg(pkg, source, dest string, opts ...Option) error {
	if err := deletePkg(pkg, source, dest); err != nil {
		return fmt.Errorf("delete step in restow failed: %v", err)
	}

	osFs := afero.NewOsFs().(afero.OsFs)
	fs := NewStowFS(osFs, dest, source)
	stow := NewStow(fs, source, dest, opts...)
	if err := stow.Stow(pkg); err != nil {
		return fmt.Errorf("stow step in restow failed: %v", err)
	}
	return nil
}
