// vagabond - Reimplementation of GNU Stow with atomic stowing (atomic stowing
// still needs to be implemented!).
//
// Vagabond implements the features of GNU Stow with the addition that stowing
// is atomic. This means that if for some reason stowing fails halfway through
// nothing will be changed.
//
// Incompatibilities with GNU Stow
// ===============================
//
// Feature set
// -----------
//
// Currently some features of stow are not implemented, namely:
//
// - Adopting
// - Folding
//
// Adopting changes the stow package and whilst potentially useful when starting
// out, I like the idea of the stow directory being immutable from vagabonds
// point of view. We'll see how this idea progresses as things continue.
//
// Folding creates symlinked directories (instead of files) when the directory
// doesn't exist. It's known as "tree folding" because the whole subtree is
// folded into the symlink. This will be super useful if I start using vagabond
// to manage source installations but as a config manager this feature isn't
// that important.
//
// Regexps
// -------
//
// GNU Stow uses PCRE regexps where as vagabond uses Go regexps. PCRE regexps
// are more extensive but come at the cost of being slower.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"

	"github.com/spf13/afero"
)

var (
	// Name of the application.
	name = "vagabond"
	// Version of this build. This variable is set from the most recent git
	// tag during the release process build.
	version = "develop"
	// Commit SHA used for this build. This variable is set during the
	// release process build.
	commit = "develop"
	// Date the binary was built.
	date = "unknown"
	// Who the binary was built by.
	builtBy = "unknown"
)

// Run based on the config given by the command line arguments.
//
// The order of actions is:
//     1. Restow packages
//     2. Delete packages
//     3. Stow packages
func run(config Config) error {
	for _, pkg := range config.RestowPackages {
		source := path.Join(config.WorkingDir, pkg)
		dest := config.TargetDir
		if err := restowPkg(pkg, source, dest, Ignore(config.Ignore), Defer(config.Defer), Override(config.Override), HandleDotFiles()); err != nil {
			return fmt.Errorf("failed to restow %s into %s: %w", pkg, dest, err)
		}
	}

	for _, pkg := range config.DeletePackages {
		source := path.Join(config.WorkingDir, pkg)
		dest := config.TargetDir
		if err := deletePkg(pkg, source, dest); err != nil {
			return fmt.Errorf("failed to delete %s from %s: %w", pkg, dest, err)
		}
	}

	osFs := afero.NewOsFs().(afero.OsFs)
	fs := NewStowFS(osFs, config.TargetDir, config.WorkingDir)
	stow := NewStow(fs, config.WorkingDir, config.TargetDir, Ignore(config.Ignore), Defer(config.Defer), Override(config.Override), HandleDotFiles())
	for _, pkg := range config.StowPackages {
		dest := config.TargetDir
		if err := stow.Stow(pkg); err != nil {
			return fmt.Errorf("failed to stow %s into %s: %w", pkg, dest, err)
		}
	}

	return nil
}

func main() {
	flagSet := flag.NewFlagSet(name, flag.ExitOnError)
	config, err := getConfigFromCmdline(flagSet, os.Args)
	if err != nil {
		fail("Error in arguments: %v", err)
	}

	if config.ShowVersion {
		fmt.Fprintf(os.Stderr, "%s %s\nCommit: %s\nBuilt at %s by %s\n", name, version, commit[:7], date, builtBy)
	}

	if config.Verbose {
		fmt.Fprintln(os.Stderr, config.String())
	} else {
		log.SetOutput(ioutil.Discard)
	}

	if err := run(config); err != nil {
		fail("Everything was rolled back as stow failed: %v", err)
	}
}
