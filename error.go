package main

import (
	"errors"
)

var (
	// ErrNotImplemented is an error returned if something is not
	// implemented
	ErrNotImplemented = errors.New("Not implemented")
)
