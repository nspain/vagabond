package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"strings"
)

// MultiValueString is an implementation of `flag.Value` that allows flags that
// use this to be used multiple times and the values appended to the slice.
type MultiValueString []string

// String representation of MultiValueString.
func (m *MultiValueString) String() string {
	return strings.Join(*m, ", ")
}

// Set the value of MultiValueString. This is where the magic happens and the
// value is just appended to the slice.
func (m *MultiValueString) Set(value string) error {
	*m = append(*m, value)
	return nil
}

// Config is the run config for vagabond.
type Config struct {
	// Do a dry run. Log what would be changed, but don't change anything.
	DryRun bool
	// Directory to base all relative paths from. This mainly applies to the
	// stow and delete pages.
	WorkingDir string
	// Directory to target as part of the stow and delete process.
	TargetDir string
	// Packages to stow.
	StowPackages []string
	// Packages to delete (unstow).
	DeletePackages []string
	// Packages to restow
	RestowPackages []string
	// Regexp for the ends of files to ignore.
	Ignore *regexp.Regexp
	// Regexp for files to ignore if they're registered in another stow
	// package.
	Defer *regexp.Regexp
	// Regexp for files to override.
	Override *regexp.Regexp
	// Enable special dot file behavior.
	DotFiles bool
	// Verbose logging
	Verbose bool
	// Show the version and other build information
	ShowVersion bool
}

// String representation of config. Useful for verbose mode.
func (c Config) String() string {
	return fmt.Sprintf(
		`
Dry run:            %v
Working directory:  %s
Target directory:   %s
Packages to stow:   [%s]
Packages to delete: [%s]
Packages to restow: [%s]
Ignore regexp:      %v
Defer regexp:       %v
Override regexp:    %v
Handle dotfiles:    %v
`,
		c.DryRun,
		c.WorkingDir,
		c.TargetDir,
		strings.Join(c.StowPackages, ", "),
		strings.Join(c.DeletePackages, ", "),
		strings.Join(c.RestowPackages, ", "),
		c.Ignore,
		c.Defer,
		c.Override,
		c.DotFiles,
	)
}

func getConfigFromCmdline(flagSet *flag.FlagSet, args []string) (Config, error) {

	no := flagSet.Bool("no", false, "Dry run.")

	// Get the current working directory to use as the default argument of
	// `dir`. If there's an issue, we log if so the user knows but don't
	// fail because all is well if an argument is provided for `dir`.
	var workingDirParent string
	workingDir, err := os.Getwd()
	if err != nil {
		log.Printf("Failed to get working directory: %v", err)
		workingDir = ""
		workingDirParent = ""
	} else {
		workingDirParent = path.Dir(workingDir)
	}

	dir := flagSet.String("dir", workingDir, "Set the stow directory")
	target := flagSet.String("target", workingDirParent, "Set the target directory")
	verbose := flagSet.Bool("verbose", false, "Send log output to stderr")
	var stows, deletes, restows MultiValueString
	flagSet.Var(&stows, "stow", "Packages to stow (can also be passed as arguments)")
	flagSet.Var(&deletes, "delete", "Packages to unstow")
	flagSet.Var(&restows, "restow", "Packages to restow")
	// adopt := flagSet.Bool("adopt", false, "Adopt unregistered files into the stow package")
	// noFolding := flagSet.Bool("no-folding", false, "Disable folding of stowed directories")
	ignore := flagSet.String("ignore", "", "Don't stow files ending in this regex")
	deferFiles := flagSet.String("defer", "", "Don't stow files beginning with this regex if they're already stowed")
	override := flagSet.String("override", "", "Force stow files beginning with this regex")
	dotfiles := flagSet.Bool("dotfiles", false, "Replace 'dot-' prefix with '.'")
	showVersion := flagSet.Bool("version", false, "Show version")

	flagSet.Parse(args[1:])

	// Compile the regexps so they're easier and quicker to use many times
	ignoreCompiled, err := regexp.Compile(*ignore)
	if err != nil {
		return Config{}, fmt.Errorf("Ignore's regexp is not valid: %w", err)
	}
	deferCompiled, err := regexp.Compile(*deferFiles)
	if err != nil {
		return Config{}, fmt.Errorf("Defer's regexp is not valid: %v", err)
	}
	overrideCompiled, err := regexp.Compile(*override)
	if err != nil {
		return Config{}, fmt.Errorf("Override's regexp is not valid: %v", err)
	}

	// Packages to stow can also be passed in as arguments.
	stows = append(stows, flagSet.Args()...)
	if len(stows) == 0 && len(deletes) == 0 && len(restows) == 0 {
		return Config{}, errors.New("No packages to stow, delete, or restow provided")
	}

	return Config{
		DryRun:         *no,
		WorkingDir:     *dir,
		TargetDir:      *target,
		StowPackages:   stows,
		DeletePackages: deletes,
		RestowPackages: restows,
		Ignore:         ignoreCompiled,
		Defer:          deferCompiled,
		Override:       overrideCompiled,
		DotFiles:       *dotfiles,
		Verbose:        *verbose,
		ShowVersion:    *showVersion,
	}, nil
}
